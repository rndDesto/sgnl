/* eslint-disable no-console */
import { exec } from "child_process";
import fs from "fs";

let NPM_TOKEN;

fs.unlink(".npmrc", (err) => {
  if (err) {
    console.log("Delete File Fail.");
  } else {
    console.log("Delete File successfully.");
  }
});

fs.readFile(".env", "binary", (err, data) => {
  if (err) {
    console.error(err);
    return;
  }
  const result = data.split("\n").map((e) => {
    const [key, value] = e.split("=");
    return { key, value };
  });
  NPM_TOKEN = result.find(({ key }) => key === "NPM_TOKEN").value;
  const arrayList = [
    "@sgnl:registry=https://gitlab.com/api/v4/projects/47865110/packages/npm/",
    `//gitlab.com/api/v4/projects/47865110/packages/npm/:_authToken=${NPM_TOKEN}`,
    "always-auth=true",
    "auto-install-peers=true",
    "legacy-peer-deps=true"
  ];
  arrayList.forEach((e) => exec(`echo ${e}  >> .npmrc`));
  console.log(".npmrc is created, ready to getting private package");
});
