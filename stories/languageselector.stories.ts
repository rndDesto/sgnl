import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import { withActions } from '@storybook/addon-actions/decorator'
import '../src/components/language-selector/language-selector'

export default {
  title: 'Selection Control/Language Selector',
  parameters: {
    actions: {
      handles: ['onClick'],
    },
    docs: {
      description: {
        component: 'Element that allows users to choose the language in which they want to view the content of a software application, website, or other digital interface.',
      },
      story: { autoplay: true },
    },
  },
  decorators: [withActions],
  argTypes: {
    class: {
      description: 'The `class` attribute is often used to point to a class name in a style sheet. It can also be used by a JavaScript to access and manipulate elements with the specific class name',
      type: 'string'
    },
    country: {
      control: { type: 'radio' },
      defaultValue: { summary: 'ID' },
      description: 'Language selector country. One of type: `ID`, `US`',
      options: ['ID', 'US'],
    },
    flagIcon: {
      control: { type: 'radio' },
      defaultValue: { summary: 'indonesia' },
      description: 'Language selector country. One of type: `indonesia`, `us`',
      options: ['indonesia', 'us'],
    },
    chevron: {
      defaultValue: { summary: false },
      description: 'Language selector chevron. If it is enabled, chevron icon will be shown next to label',
      type: 'boolean'
    }
  },
  render: (args) => html`<signal-language-selector country=${args.country} flagIcon=${args.flagIcon} ?chevron=${args.chevron}></signal-language-selector>`,
} as Meta

export const Default: StoryObj = {
  name: 'Default',
  args: {
    country: 'ID',
    flagIcon: 'indonesia',
    chevron: false
  },
}