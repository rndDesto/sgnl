import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import { withActions } from '@storybook/addon-actions/decorator'
import '../src/components/text-field/textfield'

export default {
    title: 'Input/Text Field',
    parameters: {
        actions: {
            handles: ['onChange', 'onClickIconInfo'],
        },
        docs: {
            description: {
                component: 'A text field is an input area on a form or web page where a user can type in text. It typically consists of a rectangular box with a blinking cursor indicating where text can be entered.',
            },
            story: { autoplay: true },
        },
    },
    decorators: [withActions],
    argTypes: {
        class: {
            description: 'The `class` attribute is often used to point to a class name in a style sheet. It can also be used by a JavaScript to access and manipulate elements with the specific class name',
            type: 'string'
        },
        label: {
            description: 'Text field label. It should be string',
            type: 'string'
        },
        isError: {
            defaultValue: { summary: false },
            description: 'Error text field. If it is set as true, error state will be shown',
            type: 'boolean'
        },
        isValid: {
            defaultValue: { summary: false },
            description: 'Valid text field. If it is set as true, valid state will be shown',
            type: 'boolean'
        },
        isLoading: {
            defaultValue: { summary: false },
            description: 'Loading text field. If it is set as `true`, loading state will be shown',
            type: 'boolean'
        },
        disabled: {
            defaultValue: { summary: false },
            description: "Disabled text field. If text area is disabled, it can't be clicked",
            type: 'boolean'
        },
        showInfo: {
            defaultValue: { summary: false },
            description: 'Show info in text area. If it is set as `true`, info icon will be shown',
            type: 'boolean'
        },
        isMandatory: {
            defaultValue: { summary: false },
            description: 'Mandatory text field. If it is set as `true`, mandatory status will be shown',
            type: 'boolean'
        },
        placeholder: {
            description: 'Placeholder text area. It should be string',
            type: 'string'
        },
        isPassword: {
            defaultValue: { summary: false },
            description: 'Mandatory text field. If it is set as `true`, mandatory status will be shown',
            type: 'boolean'
        },
        withoutLabel: {
            defaultValue: { summary: false },
            description: 'Text field without label. If it is set as `true`, label will be hidden',
            type: 'boolean'
        },
    },
    render: (args) => html`<signal-textfield label=${args.label} ?isError=${args.isError} ?isValid=${args.isValid} ?isLoading=${args.isLoading} ?disabled=${args.disabled} ?showInfo=${args.showInfo} ?isMandatory=${args.isMandatory} placeholder=${args.placeholder} ?isPassword=${args.isPassword} ?withoutLabel=${args.withoutLabel}></signal-textfield>`,
} as Meta

export const Default: StoryObj = {
    name: 'Default',
    args: {
        label: 'Username',
        isError: false,
        isValid: false,
        isLoading: false,
        disabled: false,
        showInfo: false,
        isMandatory: true,
        placeholder: 'Masukkan username anda',
        isPassword: false,
        withoutLabel: false,
    },
}