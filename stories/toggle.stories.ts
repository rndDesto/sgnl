import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import { withActions } from '@storybook/addon-actions/decorator'
import '../src/components/toggle/toggle'

export default {
  title: 'Selection Control/Toggle',
  parameters: {
    actions: {
      handles: ['onClick'],
    },
    docs: {
      description: {
        component: 'Element that allows users to switch between two states or modes, typically on or off. It consists of a small switch or button that can be toggled on or off by clicking or tapping on it.',
      },
      story: { autoplay: true },
    },
  },
  argTypes: {
    class: {
      description: 'The `class` attribute is often used to point to a class name in a style sheet. It can also be used by a JavaScript to access and manipulate elements with the specific class name',
      type: 'string'
    },
    checked: {
      defaultValue: { summary: false },
      description: 'Checked toggle. If it is set as true, checked state will be shown',
      type: 'boolean'
    },
    disabled: {
      defaultValue: { summary: false },
      description: "Disabled toggle. If it is set as true, toggle can't be clicked",
      type: 'boolean'
    }
  },
  decorators: [withActions],
  render: (args) => html`<signal-toggle ?checked=${args.checked} ?disabled=${args.disabled}></signal-toggle>`,
} as Meta

export const Default: StoryObj = {
  name: 'Default',
  args: {
    checked: false,
    disabled: false
  },
}