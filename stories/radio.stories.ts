import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import { withActions } from '@storybook/addon-actions/decorator'
import '../src/components/radio/radio'

export default {
  title: 'Selection Control/Radio Button',
  parameters: {
    actions: {
      handles: ['onClick'],
    },
    docs: {
      description: {
        component: 'Element that allows the user to select a single option from a set of mutually exclusive choices. It consists of a small circle or dot that can be filled in or left empty, depending on whether the user wants to select the option or not. When a radio button is selected, it is typically indicated by a filled-in circle or dot.',
      },
      story: { autoplay: true },
    },
  },
  decorators: [withActions],
  argTypes: {
    class: {
      description: 'The `class` attribute is often used to point to a class name in a style sheet. It can also be used by a JavaScript to access and manipulate elements with the specific class name',
      type: 'string'
    },
    label: {
      description: 'Radio label. It should be string',
      type: 'string'
    },
    checked: {
      defaultValue: { summary: false },
      description: 'Checked radio. If checked is set to `true`, radio will be filled by blue background color',
      type: 'boolean'
    },
    disabled: {
      defaultValue: { summary: false },
      description: "Disabled radio. If disabled is set to `true`, radio can't be clicked",
      type: 'boolean'
    },
  },
  render: (args) => html`<signal-radio ?checked=${args.checked} ?disabled=${args.disabled}>${args.label}</signal-radio>`,
} as Meta

export const Default: StoryObj = {
  name: 'Default',
  args: {
    label: 'Hey, this is radio label',
    checked: false,
    disabled: false,
  },
}