import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import { withActions } from '@storybook/addon-actions/decorator'
import '../src/components/text-area/textarea'

export default {
    title: 'Input/Text Area',
    parameters: {
        actions: {
            handles: ['onClickIconInfo', 'onChange'],
        },
        docs: {
            description: {
                component: 'A text area is a user interface element that allows for the input and display of multiple lines of text. It is often used in web forms or other applications where users need to enter or edit larger amounts of text, such as comments, messages, or descriptions.',
            },
            story: { autoplay: true },
        },
    },
    argTypes: {
        class: {
            description: 'The `class` attribute is often used to point to a class name in a style sheet. It can also be used by a JavaScript to access and manipulate elements with the specific class name',
            type: 'string'
        },
        label: {
            description: 'Text area label. It should be string',
            type: 'string'
        },
        isError: {
            description: 'Error text area. If it is set as `true`, error state will be shown',
            type: 'boolean'
        },
        errorText: {
            description: 'Text area error text. It should be string',
            type: 'string'
        },
        isValid: {
            description: 'Valid text area. If it is set as `true`, valid state will be shown',
            type: 'boolean'
        },
        validText: {
            description: 'Text area valid text. It should be string',
            type: 'string'
        },
        disabled: {
            defaultValue: { summary: false },
            description: "Disabled text area. If text area is disabled, it can't be clicked",
            type: 'boolean',
        },
        showInfo: {
            defaultValue: { summary: false },
            description: "Show info in text area. If it is set as `true`, info icon will be shown",
            type: 'boolean',
        },
        isMandatory: {
            defaultValue: { summary: false },
            description: "Mandatory text area. If it is set as `true`, mandatory status will be shown",
            type: 'boolean',
        },
        placeholder: {
            description: 'Placeholder text area. It should be string',
            type: 'string'
        },
        withoutLabel: {
            defaultValue: { summary: false },
            description: "Text area without label. If it is set as `true`, label will be hidden",
            type: 'boolean',
        },
        maxLength: {
            defaultValue: { summary: 200 },
            description: 'Max length character in text area. It should be number',
            type: 'number'
        }
    },
    decorators: [withActions],
    render: (args) => html`<signal-textarea label=${args.label} ?isError=${args.isError} errorText=${args.errorText} ?isValid=${args.isValid} validText=${args.validText} ?disabled=${args.disabled} ?showInfo=${args.showInfo} ?isMandatory=${args.isMandatory} placeholder=${args.placeholder} ?withoutLabel=${args.withoutLabel} maxLength=${args.maxLength}></signal-textarea>`,
} as Meta

export const Default: StoryObj = {
    name: 'Default',
    args: {
        label: 'Bio',
        isError: false,
        errorText: 'Error text',
        isValid: false,
        validText: 'Valid text',
        disabled: false,
        showInfo: false,
        isMandatory: false,
        placeholder: 'Masukkan bio anda',
        withoutLabel: false,
        maxLength: 200
    },
}