# Signal UI

> IMPORTANT: Signal UI is a work in progress and subject to major changes
> until 1.0 release.

Signal UI is built using Web Component and Lit Element.

## Roadmap

**Alpha** components are in-development and may have many frequent breaking
changes.

**Beta** components are mostly polished and ready for use.

**Stable** components are reviewed, documented, and API complete.

-   ❌ Not started
-   🟡 In progress
-   ✅ Complete

### 1.0 Components

No | Component          | Alpha | Beta | Stable
:-:|------------------- | :---: | :--: | :----:
1|Banner                | ✅     | 🟡    | ❌
2|Bottom Sheet & Modal  | ❌     | ❌    | ❌
3|Breadcrumbs           | ✅     | 🟡    | ❌
4|Button                | ✅     | ✅    | ❌
5|Callout               | ✅     | 🟡    | ❌
6|Chips                 | ✅     | 🟡    | ❌
7|Dialogs               | ✅     | 🟡    | ❌
8|Filters               | ❌     | ❌    | ❌
9|Footer                | ❌     | ❌    | ❌
10|Header Hero          | ❌     | ❌    | ❌
11|Label                | ❌     | ❌    | ❌
12|Menu                 | ❌     | ❌    | ❌
13|Navigation           | ❌     | ❌    | ❌
14|Quick Links          | ❌     | ❌    | ❌
15|Search               | ❌     | ❌    | ❌
16|Selection Control    | ✅     | 🟡    | ❌
17|Snackbar             | ✅     | 🟡    | ❌
18|Spacing              | ❌     | ❌    | ❌
19|Tab                  | ✅     | 🟡    | ❌
20|Text Field           | ✅     | 🟡    | ❌
21|Tooltip              | ❌     | ❌    | ❌
22|Top Bar              | ❌     | ❌    | ❌
23|Uploader             | ❌     | ❌    | ❌

## Framework

- [Angular](https://angular.io/)
- [React](https://react.dev/)

**npm:**

```sh
npm install signal-ui
```

## Getting started in Angular

Since it is using custom web component, we need to register it on `app.module.ts` file:

```
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import 'signal-ui'; --> add this

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA], --> add this
  bootstrap: [AppComponent]
})
export class AppModule { }

```

Then, you can use the component in html file. The format is `<signal-${component-name}/>`:
```
<!DOCTYPE html>
<html lang="en">
  <body>
    <signal-button variant="primary" size="xl" disabled (onClick)="() => window.alert('test')">BELI PULSA</signal-button>
  </body>
</html>
```


## Getting started in React

No additional configuration needed, you can consume it to js/ts file:
```
import React from 'react';
import { SignalButtonReact } from 'signal-ui';

function App() {
  return (
    <div>
      <SignalButtonReact variant='primary' size='xl' disabled onClick={() => window.alert('test')}>BELI PULSA</SignalButtonReact>
    </div>
  );
}

export default App;
```

## Prefix Component Name
Here is the list of prefix component name that is ready to be used:

No | Component         | Angular | React
:-:|-------------------| :-----: | :-----:
1|Badge                | signal-badge              | SignalBadgeReact
2|Banner               | signal-banner             | SignalBannerReact
3|Breadcrumb           | signal-textfield          | SignalBreadcrumbReact
4|Button               | signal-button             | SignalButtonReact
5|Callout              | signal-callout            | SignalCalloutReact
6|Checkbox             | signal-checkbox           | SignalCheckboxReact
7|Chips                | signal-chips              | SignalChipsReact
8|Dialogs              | signal-dialogs            | SignalDialogsReact
9|Floating Button      | signal-button-floating    | SignalButtonFloatingReact
10|Icon Button         | signal-button-icon        | SignalButtonIconReact
11|Language Selector   | signal-language-selector  | SignalLanguageSelectorReact
12|OTP                 | signal-otp                | SignalOTPReact
13|Radio               | signal-radio              | SignalRadioReact
14|Snackbar            | signal-snackbar           | SignalSnackbarReact
15|Social Media Button | signal-button-socmed      | SignalButtonSocmedReact
16|Tab                 | signal-tab                | SignalTabReact
17|Text Area           | signal-textarea           | SignalTextAreaReact
18|Text Field          | signal-textfield          | SignalTextfieldReact
19|Toggle              | signal-toggle             | SignalToggleReact