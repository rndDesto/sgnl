import { createComponent } from "@lit-labs/react";
import React from "react";
import { ButtonRipple } from "./button-ripple";

const ButtonRippleReact = createComponent({
    tagName: 'button-ripple',
    elementClass: ButtonRipple,
    react: React,
    events: {
      // onClick: 'onClick',
      onMantul:"onClickRippleButton"
    },
  });

  export default ButtonRippleReact