// import { css, html, LitElement, unsafeCSS } from 'lit';
// import { customElement, property } from 'lit/decorators.js';
// import { classMap } from 'lit-html/directives/class-map.js';
// import { styleMap } from 'lit/directives/style-map.js';
// import styles from './style.scss?inline';
// import { when } from 'lit/directives/when.js';


import { html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';

@customElement('button-ripple')
export class ButtonRipple extends LitElement {

  render() {
    return html`
      <button @click=${this.handleClick}>
        <slot></slot>
      </button>
    `;
  }
  handleClick() {
    console.log("run from lit element");
    let customEvent = new CustomEvent("clickRippleButton", {
      bubbles: true,
      composed: true,
      detail: { value: "aman jaya" }
    });
    this.dispatchEvent(customEvent);
  }

  // static styles = [
  //   css`${unsafeCSS(styles)}`
  // ]

  // @property({ type: Boolean }) 
  // disableRipple = false;
  // rippleActive = false;


  // @property({ type: String })
  // class?= '';
  // gantiBg = '';

  // @property({ attribute: false }) 
  // rippleStyle = {};

  // getRandomHexa(length: Number) {
  //   return [...Array(length)].map(() => Math.floor(Math.random() * 16).toString(16)).join('');
  // }


  // handleClick(event: Event): void {
  //   console.log("handleClick dari lit")
    
  //   this.gantiBg = this.getRandomHexa(6);

  //   if (!this.disableRipple) {
  //     const button = event.currentTarget as HTMLElement;
  //     const diameter = Math.max(button.clientWidth, button.clientHeight);
  //     const radius = diameter / 2;
  //     const left = (event as MouseEvent).clientX - button.getBoundingClientRect().left - radius;
  //     const top = (event as MouseEvent).clientY - button.getBoundingClientRect().top - radius;

  //     this.rippleStyle = {
  //       width: `${diameter}px`,
  //       height: `${diameter}px`,
  //       left: `${left}px`,
  //       top: `${top}px`
  //     };
  //     this.rippleActive = true;
  //   }
  //   let customEvent = new CustomEvent('clickRippleButton', { bubbles: true, composed: true, detail: {'value': 'aman jaya'}});
  //   this.dispatchEvent(customEvent);
  // }

  // render() {
  //   const classes = classMap({
  //     [this.class!]: !!this.class,
  //     "ripple-button": true
  //   });

  //   const rippleClass = classMap({
  //     "ripple": this.rippleActive
  //   });
    

  //   return html`
  //   <button 
  //     class=${classes}
  //     @click=${this.handleClick}
  //     style=${styleMap({backgroundColor: `#${this.gantiBg}`})}
  //   >
  //     <slot></slot>
  //     ${when(!this.disableRipple, () => html`<span 
  //     class=${rippleClass}
  //     style=${styleMap(this.rippleStyle)}
  //   ></span>`)}
  //   </button>
  //   `;
  // }
}

declare global {
  interface HTMLElementTagNameMap {
    'button-ripple': ButtonRipple
  }
}
