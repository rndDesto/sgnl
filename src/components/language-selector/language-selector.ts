import { createComponent } from '@lit-labs/react';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { when } from 'lit/directives/when.js';
import React from 'react';
import globalStyles from '../../global.css?inline';
import styles from './language-selector.scss?inline';

@customElement('signal-language-selector')
export class SignalLanguageSelector extends LitElement {
    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]

    /**
    * Country variant. Default is ID.
    */
    @property({ reflect: true })
    country: 'ID' | 'US' = 'ID';
    /**
    * Flag icon variant. Default is indonesia.
    */
    @property({ reflect: true })
    flagIcon: 'indonesia' | 'us' = 'indonesia';
    /**
    * Language selector chevron.
    */
    @property({ type: Boolean })
    chevron?= false;
    /**
    * Language selector class.
    */
    @property({ type: String })
    class?= '';

    private onClick() {
        const event = new CustomEvent('onClick', { bubbles: true, composed: true });
        this.dispatchEvent(event);
    }
    render() {
        return html`
        <div class="language-selector label-bold ${this.class}" @click=${this.onClick}>
            <span class="icon-country-${this.flagIcon}"></span>
            <div class="mr-4"></div>
            <p>${this.country}</p>
            ${when(this.chevron, () => html`<span class="ml-8 chevron-right"></span>`)}
        </div>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-language-selector': SignalLanguageSelector
    }
}

export const SignalLanguageSelectorReact = createComponent(
    React,
    'signal-language-selector',
    SignalLanguageSelector,
    {
        onClick: 'onClick'
    }
)