import { createComponent } from '@lit-labs/react';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import React from 'react';
import globalStyles from '../../global.css?inline';
import styles from './checkbox.scss?inline';
import { classMap } from 'lit-html/directives/class-map.js';

@customElement('signal-checkbox')
export class SignalCheckbox extends LitElement {
    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]

    /**
    * Checked checkbox.
    */
    @property({ type: Boolean })
    checked?= false;
    /**
    * Disabled checkbox.
    */
    @property({ type: Boolean })
    disabled?= false;
    /**
    * Checkbox class.
    */
    @property({ type: String })
    class?= '';
    /**
    * Checkbox label position.
    */
    @property({ reflect: true })
    labelPosition: 'left' | 'right' = 'right';

    private onChange() {
        const event = new CustomEvent('onChange', { bubbles: true, composed: true });
        this.dispatchEvent(event);
    }

    render() {
        const checkboxStyle = classMap({
            [this.class!]: this.class!,
            'container-checkbox body01-regular': true,
            'checkbox-left': this.labelPosition == 'left',
            'checkbox-right': this.labelPosition == 'right'
        });

        return html`
            <div class=${checkboxStyle}>
                <label>
                    <input id="checkbox" type="checkbox" ?checked=${this.checked} ?disabled=${this.disabled} @click=${this.onChange}/>
                    <slot></slot>
                </label>
            </div>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-checkbox': SignalCheckbox
    }
}

export const SignalCheckboxReact = createComponent(
    React,
    'signal-checkbox',
    SignalCheckbox,
    {
        onChange: 'onChange'
    }
)