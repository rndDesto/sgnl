import { createComponent } from '@lit-labs/react';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import React from 'react';
import globalStyles from '../../global.css?inline';
import styles from './radio.scss?inline';

@customElement('signal-radio')
export class SignalRadio extends LitElement {
    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]

    /**
    * Checked radio.
    */
    @property({ type: Boolean })
    checked?= false;
    /**
    * Disabled radio.
    */
    @property({ type: Boolean })
    disabled?= false;
    /**
    * Radio class.
    */
    @property({ type: String })
    class?= '';

    private onClick() {
        const event = new CustomEvent('onClick', { bubbles: true, composed: true });
        this.dispatchEvent(event);
    }

    render() {
        return html`
        <div class="container-radio body01-regular ${this.class}">
            <input id="radio" type="radio" ?checked=${this.checked} ?disabled=${this.disabled} @click=${this.onClick}/>
            <label for="radio">
                <slot></slot>
            </label>
        </div>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-radio': SignalRadio
    }
}

export const SignalRadioReact = createComponent(
    React,
    'signal-radio',
    SignalRadio,
    {
        onClick: 'onClick'
    }
)