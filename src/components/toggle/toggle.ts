import { createComponent } from '@lit-labs/react';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import React from 'react';
import globalStyles from '../../global.css?inline';
import styles from './toggle.scss?inline';
import { ifDefined } from 'lit/directives/if-defined.js';

@customElement('signal-toggle')
export class SignalToggle extends LitElement {
    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]

    /**
    * Checked radio.
    */
    @property({ type: Boolean })
    checked?= false;
    /**
    * Disabled radio.
    */
    @property({ type: Boolean })
    disabled?= false;
    /**
    * Toggle class.
    */
    @property({ type: String })
    class?= '';

    private onClick() {
        const event = new CustomEvent('onClick', { bubbles: true, composed: true });
        this.dispatchEvent(event);
    }

    render() {
        return html`
        <label class="switch-tsel ${ifDefined(this.class)}">
            <input type="checkbox" ?checked=${this.checked} ?disabled=${this.disabled} @click=${this.onClick}/>
            <span class="slider round"></span>
        </label>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-toggle': SignalToggle
    }
}

export const SignalToggleReact = createComponent(
    React,
    'signal-toggle',
    SignalToggle,
    {
        onClick: 'onClick'
    }
)