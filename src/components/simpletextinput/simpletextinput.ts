import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

@customElement('simple-text-input')
export class SimpleTextInput extends LitElement {
  @property({ type: String }) value = '';
  @property({ type: Function }) onvaluechange = (val: any) => {return val};

  static styles = css`
    input {
      padding: 8px;
      font-size: 16px;
      border: 1px solid #ccc;
      border-radius: 4px;
    }
  `;

  private onInput(event: Event) {
    const inputElement = event.target as HTMLInputElement;
    this.value = inputElement.value;
    this.onvaluechange(inputElement.value); 
    console.log("onvaluechange = ", inputElement.value)
    this.dispatchEvent(new CustomEvent('onvaluechanged', { detail: this.value }));
  }
  

  render() {
    return html`
      <input
        type="text"
        .value=${this.value}
        @input=${this.onInput}
      />
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'simple-text-input': SimpleTextInput
  }
}
