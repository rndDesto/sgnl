import { html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import globalStyles from '../../global.css?inline';



@customElement('signal-modal')
export class SignalModal extends LitElement {

  static styles = [
    unsafeCSS(globalStyles)
    ]

  @property({ type: Boolean }) 
  isOpen = false;


  @property()
  class?= '';

  @property() 
  innerStyle? = '';


renderModal(){
console.log("ssss = ", this.class!)

    return html`
    <div>
        <div part="modalWrapper" class="fixed inset-0 bg-black bg-opacity-50 flex justify-center items-center mantuljaya" id="myModal" data-testid="modal-lit" style=${this.innerStyle}>
            <div class="bg-white p-6 rounded shadow-lg w-96 iniMantul">
                <div>
                    <slot></slot>
                </div>
                <div class=${this.class}>
                    <button onclick="closeModal()" class="bg-red-500 text-white px-4 py-2 rounded w-full">Tutup</button>
                </div>
            </div>
        </div>
    </div>
    `;
}

  render() {
    return this.renderModal()
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'signal-modal': SignalModal
  }
}
