import AppleIcon from './ico_apple.svg';
import CalendarIcon from './ico_calendar.svg';
import CartIcon from './ico_cart.svg';
import CheckIcon from './ico_check.svg';
import ChevronDownIcon from './ico_chevron_down.svg';
import ChevronRightIcon from './ico_chevron_right.svg';
import CloseIcon from './ico_close.svg';
import CookiesIcon from './ico_cookies.svg';
import DeleteCircleIcon from './ico_delete_circle.svg';
import EyeCloseIcon from './ico_eye_close.svg';
import EyeOpenIcon from './ico_eye_open.svg';
import FacebookIcon from './ico_facebook.svg';
import GoogleIcon from './ico_google.svg';
import InfoIcon from './ico_info.svg';
import TwitterIcon from './ico_twitter.svg';
import PlaceholderIcon from './ico_placeholder.svg';
import SearchIcon from './ico_search.svg';
import WarningIcon from './ico_warning.svg';

export {
    AppleIcon,
    CalendarIcon,
    CartIcon,
    CheckIcon,
    ChevronDownIcon,
    ChevronRightIcon,
    CloseIcon,
    CookiesIcon,
    DeleteCircleIcon,
    EyeCloseIcon,
    EyeOpenIcon,
    FacebookIcon,
    GoogleIcon,
    InfoIcon,
    TwitterIcon,
    PlaceholderIcon,
    SearchIcon,
    WarningIcon
};
