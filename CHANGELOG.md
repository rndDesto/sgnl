

### 0.0.159 (2023-08-07)


### Bug Fixes

* **.gitlab-ci.yaml:** tambah script npmrc ([c998656](https://gitlab.com/rndDesto/sgnl/commit/c998656742db5478be7e0f6a8c026473fdfc3ccb))
* .nv dinamis dari yaml ([1f473f6](https://gitlab.com/rndDesto/sgnl/commit/1f473f650a4686aa935d9140b58a393b770a9ac4))
* coba modal ([cb65f69](https://gitlab.com/rndDesto/sgnl/commit/cb65f693b9f410e04743267ca493933fc5d442d0))
* delet import modal ([ccdae14](https://gitlab.com/rndDesto/sgnl/commit/ccdae14da01da33a3d61bfac819d9f39169d8443))
* tambah scrip evn ([59c5070](https://gitlab.com/rndDesto/sgnl/commit/59c50705ed44067a836fe270429856f3c7cb5205))
* tambah script npmrc ([61b3275](https://gitlab.com/rndDesto/sgnl/commit/61b3275ba2d66a90ae04b9a7755048e298a7c258))
* tambah yaml gitlab ([aa4af4e](https://gitlab.com/rndDesto/sgnl/commit/aa4af4e607f53fd2b57174eab1c46ad7df5ed462))

### 0.0.158 (2023-07-31)


### Bug Fixes

* manual ubah ver ([ccc63cd](https://gitlab.com/rndDesto/sgnl/commit/ccc63cd908871b1c520a52795d9e1827497ebae4))
* tambah file export dts ([7311090](https://gitlab.com/rndDesto/sgnl/commit/731109085ba927abcd66995f2f99f64694e0e6d8))

### 0.0.156 (2023-07-31)


### Bug Fixes

* cb typesafe react ([31951c9](https://gitlab.com/rndDesto/sgnl/commit/31951c9571e50eb4344d63150f1a734725b838b4))

### 0.0.155 (2023-07-28)


### Bug Fixes

* ganti code ripple ([617c0f9](https://gitlab.com/rndDesto/sgnl/commit/617c0f902fcdafc368228789194a8e499b3df7c2))

### 0.0.154 (2023-07-28)


### Bug Fixes

* coba commit ([37065ab](https://gitlab.com/rndDesto/sgnl/commit/37065ab19d2757e8942e3c2d159a2885c6c1a9f6))
* coba ganti handle log ([c84121a](https://gitlab.com/rndDesto/sgnl/commit/c84121a0775700162609464937aca22103e02b04))

### 0.0.153 (2023-07-28)


### Bug Fixes

* verb prefix vue ([027ef19](https://gitlab.com/rndDesto/sgnl/commit/027ef190539726175ab6521db49b06a0b30a6456))

### 0.0.152 (2023-07-27)


### Bug Fixes

* coba ganti nama event dispatch ([9883ebd](https://gitlab.com/rndDesto/sgnl/commit/9883ebd02e008aaf50081f3f8d4781e89624eec2))

### 0.0.151 (2023-07-26)


### Bug Fixes

* balikin componen react ([aa07119](https://gitlab.com/rndDesto/sgnl/commit/aa07119db1ae043d8c41e21951ea1796fe5ab29c))
* ganti event cngahe ([4bbe1f0](https://gitlab.com/rndDesto/sgnl/commit/4bbe1f075a4b45d857dbc4805352bb093fbbe0b6))

### 0.0.150 (2023-07-26)


### Bug Fixes

* ganti val react ([f95e5a1](https://gitlab.com/rndDesto/sgnl/commit/f95e5a12614935b7acd6471da429ef14a2203074))

### 0.0.149 (2023-07-26)


### Bug Fixes

* coba fungsi ([34d2c44](https://gitlab.com/rndDesto/sgnl/commit/34d2c446a8ce8569d204acdd4a275fc63e4f4642))
* fixing value yg blm terpanggil ([f87e453](https://gitlab.com/rndDesto/sgnl/commit/f87e4532ddda90df1b65b5a691334e7c5693753f))

### 0.0.148 (2023-07-26)


### Bug Fixes

* cb calback value ([7c1bbdb](https://gitlab.com/rndDesto/sgnl/commit/7c1bbdbc3f08da75c2deba7070cab7fee6460aa5))
* cb fix ([57f0c1a](https://gitlab.com/rndDesto/sgnl/commit/57f0c1a29d31d4024aacb842cafe0a404a7cce9e))
* coba push lagi ([75f6b0b](https://gitlab.com/rndDesto/sgnl/commit/75f6b0b5a8da242580452e187027dc84ce6ed6b9))
* ilanging error ([ea66398](https://gitlab.com/rndDesto/sgnl/commit/ea66398ad2435916fa658f43c30d5e42101991ff))
* log input ([805f6c7](https://gitlab.com/rndDesto/sgnl/commit/805f6c77372deac6b7b8632933e9ba475f5c6876))
* onvalue change ([3995437](https://gitlab.com/rndDesto/sgnl/commit/3995437470f3449d43dda54c157f46465428ed3f))

### 0.0.147 (2023-07-24)


### Bug Fixes

* callback value ([931074d](https://gitlab.com/rndDesto/sgnl/commit/931074dd1cc5f1c589b3b66b7be59f19750aeefe))

### 0.0.146 (2023-07-24)


### Bug Fixes

* log input ([d98b10a](https://gitlab.com/rndDesto/sgnl/commit/d98b10a85d234c94b8728ff264358f229334b761))

### 0.0.145 (2023-07-24)


### Bug Fixes

* fixing input ([85cb352](https://gitlab.com/rndDesto/sgnl/commit/85cb35265c0b3acfbbd62921270bb6499bbdbe05))

### 0.0.144 (2023-07-24)


### Bug Fixes

* fixing glpat ([cf1fa4a](https://gitlab.com/rndDesto/sgnl/commit/cf1fa4acfa398fb4fd292c017e6ed9155366875d))
* **fixing text input:** fixing tesxt input untuk angular type=input ([e06c894](https://gitlab.com/rndDesto/sgnl/commit/e06c894a4cff7660fa5b37cfb6c6f4906dd0d728))
* **tools:** generate npm file ([918a0c6](https://gitlab.com/rndDesto/sgnl/commit/918a0c6dd677b0924b547b9131ad4b08c6035f51))

### 0.0.142 (2023-07-21)


### Bug Fixes

* add script build ([11b0ec4](https://gitlab.com/rndDesto/sgnl/commit/11b0ec437df91f46b5b9189721830ac607f6ce83))

### 0.0.141 (2023-07-21)


### Bug Fixes

* tambah ripple button ([9f99b82](https://gitlab.com/rndDesto/sgnl/commit/9f99b82901de9f338d93cfd6c267b4c96ba2316a))

### 0.0.140 (2023-07-21)


### Bug Fixes

* ripple button ([c6620a5](https://gitlab.com/rndDesto/sgnl/commit/c6620a5871cdcb75a9ad5f0aa3053de08060e7b1))

### 0.0.139 (2023-07-21)


### Bug Fixes

* **button.js:** coba event ganda ([aa4619a](https://gitlab.com/rndDesto/sgnl/commit/aa4619a3718c6a55c2de9546da25090abe5bb485))

### 0.0.138 (2023-07-21)


### Bug Fixes

* enhance react cmp ([ea4e73f](https://gitlab.com/rndDesto/sgnl/commit/ea4e73fb1e244bc48cc15e976cd5bcaa28cbaaff))

## 0.0.137 (2023-07-20)


### Bug Fixes

* **release-it.json:** skip publish ([232b061](https://gitlab.com/rndDesto/sgnl/commit/232b061fcc4de58f0edecc640158e4e942e3ddf2))

## 0.0.136 (2023-07-20)


### Bug Fixes

* **button.ts:** edit button ([048d602](https://gitlab.com/rndDesto/sgnl/commit/048d602b7ed661fb2a14ff622334902abe5b2249))

## 0.0.135 (2023-07-20)


### Bug Fixes

* **types:** coba export types ([d63bb2e](https://gitlab.com/rndDesto/sgnl/commit/d63bb2e8386c586d5425b5f00c3ff90052fbcaae))

## 0.0.134 (2023-07-20)


### Bug Fixes

* **button.ts:** cobain ubah button ([1ac4e47](https://gitlab.com/rndDesto/sgnl/commit/1ac4e4794dff49297daff5000cd26c5149003695))

## 0.0.133 (2023-07-20)


### Bug Fixes

* config versioning ([aa5e270](https://gitlab.com/rndDesto/sgnl/commit/aa5e27061a2901cc71a3db401445d60013e1c8a1))
* edit release json ([1cc9530](https://gitlab.com/rndDesto/sgnl/commit/1cc9530c65458a73ffbbb810ef056f36501f05c4))

## 0.0.136 (2023-07-20)


### Bug Fixes

* **button.ts:** edit button ([048d602](https://gitlab.com/rndDesto/sgnl/commit/048d602b7ed661fb2a14ff622334902abe5b2249))

## 0.0.135 (2023-07-20)


### Bug Fixes

* **types:** coba export types ([d63bb2e](https://gitlab.com/rndDesto/sgnl/commit/d63bb2e8386c586d5425b5f00c3ff90052fbcaae))

## 0.0.134 (2023-07-20)


### Bug Fixes

* **button.ts:** cobain ubah button ([1ac4e47](https://gitlab.com/rndDesto/sgnl/commit/1ac4e4794dff49297daff5000cd26c5149003695))

## 0.0.133 (2023-07-20)


### Bug Fixes

* config versioning ([aa5e270](https://gitlab.com/rndDesto/sgnl/commit/aa5e27061a2901cc71a3db401445d60013e1c8a1))
* edit release json ([1cc9530](https://gitlab.com/rndDesto/sgnl/commit/1cc9530c65458a73ffbbb810ef056f36501f05c4))