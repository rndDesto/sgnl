import { create } from '@storybook/theming/create';

export default create({
  base: 'light',

  brandTitle: 'Signal UI Storybook',
  brandUrl: 'https://signal-storybook-dff2a.web.app/',
  brandImage: 'https://signal-design-system.web.app/_next/static/media/signal_logo.a206e142.png',
  brandTarget: '_self',

  colorPrimary: '#FF0025',
  colorSecondary: '#001A41',

  textColor: '#001A41',
});