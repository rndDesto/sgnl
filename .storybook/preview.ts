import { Preview } from '@storybook/web-components';

const preview: Preview = {
  parameters: {
    options: {
      storySort: {
        order: ['Banner', 'Buttons', 'Input', 'Navigation', 'Overlay', 'Selection Control'],
      },
    },
  },
};

export default preview;
